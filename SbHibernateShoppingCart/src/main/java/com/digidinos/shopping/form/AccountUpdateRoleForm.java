package com.digidinos.shopping.form;

import java.io.IOException;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.digidinos.shopping.entity.Account;

public class AccountUpdateRoleForm {private int id;
private String userName;
private String userRole;
private String desception;
private boolean newAccount;

public AccountUpdateRoleForm() {
	this.newAccount = true;
}

BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

public AccountUpdateRoleForm(Account account) {
	this.userName=account.getUserName();
	this.id = account.getId();
	this.userRole = account.getUserRole();
}

public Account getAccount() throws IOException {
	Account account = new Account();
	account.setId(this.id);
	account.setUserName(this.userName);
	account.setUserRole(this.userRole);
	return account;
}



public String getUserName() {
	return userName;
}

public void setUserName(String userName) {
	this.userName = userName;
}

public String getDesception() {
	return desception;
}

public void setDesception(String desception) {
	this.desception = desception;
}

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public boolean isNewPassword() {
	return newAccount;
}

public void setNewPassword(boolean newPassword) {
	this.newAccount = newPassword;
}

public String getUserRole() {
	return userRole;
}

public void setUserRole(String userRole) {
	this.userRole = userRole;
}

public boolean isNewAccount() {
	return newAccount;
}

public void setNewAccount(boolean newAccount) {
	this.newAccount = newAccount;
}



}
