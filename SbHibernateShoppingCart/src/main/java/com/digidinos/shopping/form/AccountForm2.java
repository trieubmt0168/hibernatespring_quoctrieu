package com.digidinos.shopping.form;

import java.io.IOException;
import java.util.Date;

import javax.persistence.Id;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.multipart.MultipartFile;

import com.digidinos.shopping.entity.Account;

public class AccountForm2 {
	@Id
	private int id;
	private String userName;
	private boolean active;
	private String userRole;
	private boolean newAccount = false;
	private boolean isDelete;
	private String encrytedPassword;
	private MultipartFile fileData;

	public AccountForm2() {
		this.newAccount = true;
	}

	public AccountForm2(Account account) {
		this.id = account.getId();
		this.active = account.isActive();
		this.userName = account.getUserName();
		this.userRole = account.getUserRole();
		this.isDelete = account.isDelete();
		this.encrytedPassword = account.getEncrytedPassword();
	}

	public AccountForm2(int id, String userName, boolean active, String userRole, boolean newAccount, boolean isDelete,
			String encrytedPassword) {
		super();
		this.id = id;
		this.userName = userName;
		this.active = active;
		this.userRole = userRole;
		this.newAccount = newAccount;
		this.isDelete = isDelete;
		this.encrytedPassword = encrytedPassword;
	}

	BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

	public Account getAccount() throws IOException {

		Account account = new Account();
		account.setId(this.id);
		account.setActive(this.active);
		account.setUserName(this.userName);
		account.setUserRole(this.userRole);
//		account.setEncrytedPassword(bCryptPasswordEncoder.encode(this.encrytedPassword) );
		account.setDelete(true);
		account.setActive(true);
		account.setCreateAt(new Date());
		account.setImage(this.getFileData().getBytes());

		return account;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	public boolean isNewAccount() {
		return newAccount;
	}



	public void setNewAccount(boolean newAccount) {
		this.newAccount = newAccount;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	public String getEncrytedPassword() {
		return encrytedPassword;
	}

	public void setEncrytedPassword(String encrytedPassword) {
		this.encrytedPassword = encrytedPassword;
	}
	public MultipartFile getFileData() {
		return fileData;
	}

	public void setFileData(MultipartFile fileData) {
		this.fileData = fileData;
	}
}
