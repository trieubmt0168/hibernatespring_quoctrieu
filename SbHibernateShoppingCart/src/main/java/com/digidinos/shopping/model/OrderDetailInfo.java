package com.digidinos.shopping.model;

public class OrderDetailInfo {
	 private int id;
	 
	    private int productCode;
	    private String productName;
	 
	    private int quanity;
	    private double price;
	    private double amount;
	 
	    public OrderDetailInfo() {
	 
	    }
	 
	    // Sử dụng cho JPA/Hibernate Query.
	    public OrderDetailInfo(int id, int productCode, //
	            String productName, int quanity, double price, double amount) {
	        this.id = id;
	        this.productCode = productCode;
	        this.productName = productName;
	        this.quanity = quanity;
	        this.price = price;
	        this.amount = amount;
	    }
	 
	    public int getId() {
	        return id;
	    }
	 
	    public void setId(int id) {
	        this.id = id;
	    }
	 
	    public int getProductCode() {
	        return productCode;
	    }
	 
	    public void setProductCode(int productCode) {
	        this.productCode = productCode;
	    }
	 
	    public String getProductName() {
	        return productName;
	    }
	 
	    public void setProductName(String productName) {
	        this.productName = productName;
	    }
	 
	    public int getQuanity() {
	        return quanity;
	    }
	 
	    public void setQuanity(int quanity) {
	        this.quanity = quanity;
	    }
	 
	    public double getPrice() {
	        return price;
	    }
	 
	    public void setPrice(double price) {
	        this.price = price;
	    }
	 
	    public double getAmount() {
	        return amount;
	    }
	 
	    public void setAmount(double amount) {
	        this.amount = amount;
	    }
	}
