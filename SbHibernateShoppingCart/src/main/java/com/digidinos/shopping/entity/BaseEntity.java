package com.digidinos.shopping.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.DynamicUpdate;

@MappedSuperclass
@DynamicUpdate
public class BaseEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7809054882843006311L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID",nullable = false)
	protected int id;
	
	@Column(name = "CREATED_AT",nullable = false)
	protected Date createdAt;
	
	@Column(name = "UPDATED_AT",nullable = false)
	protected Date updatedAt;
	
	@Column(name = "DELETED_AT",nullable = true)
	protected Date DeletedAt;
	   
    @Column(name="IS_DELETE",nullable=true)
    protected boolean isDelete;
	public BaseEntity() {
		
		// TODO Auto-generated constructor stub
	}
	public BaseEntity(int id, Date createdAt, Date updatedAt, Date deletedAt,boolean isDelete) {

		this.id = id;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.DeletedAt = deletedAt;
		this.isDelete = isDelete;
	}
	public int getId() {
		return id;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	public Date getDeletedAt() {
		return DeletedAt;
	}
	public void setDeletedAt(Date deletedAt) {
		DeletedAt = deletedAt;
	}
	public boolean isDelete() {
		return isDelete;
	}
	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getCreateAt() {
		return createdAt;
	}
	public void setCreateAt(Date createAt) {
		this.createdAt = createAt;
	}
	public Date getUpdateAt() {
		return updatedAt;
	}
	public void setUpdateAt(Date updateAt) {
		this.updatedAt = updateAt;
	}
	public Date getDeleteAt() {
		return DeletedAt;
	}
	public void setDeleteAt(Date deleteAt) {
		DeletedAt = deleteAt;
	}
	
}
