package com.digidinos.shopping.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.digidinos.shopping.entity.OrderDetail;
@Repository
public interface OrderDetailRepository<ID> extends BaseRepository<OrderDetail, ID>{
	@Query(value="SELECT * FROM Order_Details o where o.order_id = :orderId", nativeQuery = true) 
	List<OrderDetail> findAllByOrderId(Integer orderId);

}
