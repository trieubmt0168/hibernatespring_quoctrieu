package com.digidinos.shopping.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.digidinos.shopping.entity.Account;
import com.digidinos.shopping.entity.Product;

@Repository
public interface ProductRepository extends BaseRepository<Product, Integer> {

	@Query(value = "SELECT t.* FROM products t where t.price = :price", nativeQuery = true)
	List<Product> findByPrice(@Param("price") int price);

	@Query(value = "select  id, image, created_at , deleted_at , updated_at , code , is_delete , name , price, description from products where is_delete = 0 order by created_at desc limit 5", nativeQuery = true)
	List<Product> top5ProductNew();

	@Query(value = "select * FROM products WHERE NAME LIKE  '%:proName%'", nativeQuery = true)
	List<Product> SearchPro(@Param("proName") String proName);

	@Query(value = "Select * from Products p where p.name like %:name% and is_delete = 0", nativeQuery = true)
	List<Product> findByName(@Param("name") String name);

	@Query(value = "Select * from Products where is_delete=0", nativeQuery = true)
	Page<Product> findAll(Pageable pageable);

	@Query(value = "select * FROM products WHERE NAME LIKE  %:name%", nativeQuery = true)
	Page<Product> findByNameProduct(@Param("name") String name, Pageable pageable);
}
