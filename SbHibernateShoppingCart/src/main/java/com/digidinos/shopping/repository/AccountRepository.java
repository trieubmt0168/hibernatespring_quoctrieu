package com.digidinos.shopping.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.digidinos.shopping.entity.Account;
import com.digidinos.shopping.entity.Product;

@Repository
public interface AccountRepository extends BaseRepository<Account, Integer> {
	Account findByUserName(String userName);

//@Query(value = "FROM Account c WHERE c.employee.name LIKE '%\" + nameEmployee + \"%'\"", nativeQuery = true)
//   	@Query(value = "select * FROM products   WHERE NAME LIKE  '% =:NameAcc%', nativeQuery = true)
//	List<Account> SearchAccount();
	
	@Query(value = "select * FROM accounts WHERE user_name LIKE  %:name%", nativeQuery = true)
	Page<Account> findByNameAccount(@Param("name") String name,Pageable pageable);
}
