package com.digidinos.shopping.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.digidinos.shopping.entity.Order;
@Repository
public interface OrderRepository extends BaseRepository<Order, Integer>{	
	@Query(value = "select max(o.order_num) from Orders o", nativeQuery = true)  
	int maxOrderNum();
	@Query(value = "select * FROM orders WHERE CUSTOMER_NAME LIKE  %:name%", nativeQuery = true)
	Page<Order> findByNameProduct(@Param("name") String name,Pageable pageable);
}
