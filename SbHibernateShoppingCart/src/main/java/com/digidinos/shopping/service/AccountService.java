package com.digidinos.shopping.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.digidinos.shopping.entity.Account;
import com.digidinos.shopping.entity.Account;
import com.digidinos.shopping.repository.AccountRepository;

@Service
public class AccountService {
	@Autowired
	AccountRepository accountRepository;

	public List<Account> findAll() {
		return this.accountRepository.findAll();

	}

	public void add(Account account) {
		this.accountRepository.save(account);

	}

	public void update(Account account) {

		Optional<Account> accountOtp = accountRepository.findById(account.getId());
		Account account2 = new Account();
		if (accountOtp.isPresent()) {
			account2 = accountOtp.get();
			account2.setUpdateAt(new Date());
			account2.setUserName(account.getUserName());
			account2.setUserRole(account.getUserRole());
		}
		this.accountRepository.save(account2);
	}

	public void updatepasword(Account account) {

		Optional<Account> accountOtp = accountRepository.findById(account.getId());
		Account account2 = new Account();
		if (accountOtp.isPresent()) {
			account2 = accountOtp.get();
			account2.setUpdateAt(new Date());
			account2.setUserName(account.getUserName());
			account2.setUserRole(account.getUserRole());
			account2.setEncrytedPassword(account.getEncrytedPassword());
		}
		this.accountRepository.save(account2);

	}

	public void delete(int id) {
		// TODO Auto-generated method stub
		this.accountRepository.deleteById(id);
	}

	public Optional<Account> selectbyid(int id) {

		return accountRepository.findById(id);

	}

	public Optional<Account> selectById(int id) {
		Optional<Account> accountOpt = accountRepository.findById(id);
		return accountOpt;
	}

	public void updateProfile(Account account) {
		Optional<Account> accountOtp = accountRepository.findById(account.getId());
		Account account2 = new Account();
		if (accountOtp.isPresent()) {
			account2 = accountOtp.get();
			account2.setUpdatedAt(new Date());
			account2.setImage(account.getImage());
		}
		this.accountRepository.save(account2);
	}

	public Page<Account> findpage(Pageable pageable) {
		return this.accountRepository.findAll(pageable);

	}
	public Page<Account> findAll(Pageable pageable) {
		return this.accountRepository.findAll(pageable);

	}

	public void updateRole(Account account) {

		Optional<Account> accountOtp = accountRepository.findById(account.getId());
		Account account2 = new Account();
		if (accountOtp.isPresent()) {
			account2 = accountOtp.get();
			account2.setUpdatedAt(new Date());
			account2.setUserRole(account.getUserRole());
		}
		this.accountRepository.save(account2);
	}

	public List<Account> bypages(int start, int limit) {

		return null;

	}

	public Optional<Account> getAccountById(int idAccount) {
		return this.accountRepository.findById(idAccount);
	}

	public Account findByUserName(String userName) {
		Account account = accountRepository.findByUserName(userName);
		return account;
	}

	public void updatePassword(Account account) {

		Optional<Account> accountOtp = accountRepository.findById(account.getId());
		Account account2 = new Account();
		if (accountOtp.isPresent()) {
			account2 = accountOtp.get();
			account2.setUpdateAt(new Date());
			account2.setEncrytedPassword(account.getEncrytedPassword());
		}
		this.accountRepository.save(account2);
	}

//	private List<Account> SearchName(String nameAccount) {
//		return (List<Account>) this.accountRepository.findByUserName(nameAccount);
//
//	}
	public Page<Account> findByNameAccount(String name, Pageable pageable) {
		Page<Account> Accounts = accountRepository.findByNameAccount(name, pageable);
		return Accounts;
	}
}
