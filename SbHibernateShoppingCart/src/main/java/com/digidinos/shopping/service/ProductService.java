package com.digidinos.shopping.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.digidinos.shopping.entity.Product;
import com.digidinos.shopping.repository.ProductRepository;

@Service
public class ProductService {
	@Autowired
	ProductRepository productRepository;

	public List<Product> findAll() {
		return this.productRepository.findAll();
	}

	public void add(Product product) {
		this.productRepository.save(product);

	}

	public void update(Product product) {
		this.productRepository.save(product);

	}

//	public void delete(int id) {
//		// TODO Auto-generated method stub
//		this.productRepository.deleteById(id);
//	}

	public Optional<Product> selectbyid(int id) {

		return productRepository.findById(id);

	}

	public Page<Product> findAll(Pageable pageable) {
		return this.productRepository.findAll(pageable);
	}

	public void delete(int id) {
		Optional<Product> productOpt = selectbyid(id);
		Product product = productOpt.get();
		product.setDelete(true);
		product.setDeleteAt(new Date());
		productRepository.save(product);
	}

	public List<Product> top5NewProduct() {
		List<Product> products = productRepository.top5ProductNew();
		return products;
	}

	public List<Product> SearchNamePro(String proName) {
		List<Product> products = productRepository.SearchPro(proName);
		return products;
	}

	public Page<Product> findByNameProduct(String name, Pageable pageable) {
		Page<Product> products = productRepository.findByNameProduct(name, pageable);
		return products;
	}

	public void updateImage(Product product) {
		Optional<Product> productOtp = productRepository.findById(product.getId());
		Product product2 = new Product();
		if (productOtp.isPresent()) {
			product2 = productOtp.get();
			product2.setUpdatedAt(new Date());
			product2.setCode(product.getCode());
			product2.setName(product.getName());
			product2.setPrice(product.getPrice());
			product2.setImage(product.getImage());
		}
		this.productRepository.save(product2);
	}
}
