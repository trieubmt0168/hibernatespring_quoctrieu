package com.digidinos.shopping.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.digidinos.shopping.entity.Order;
import com.digidinos.shopping.entity.OrderDetail;
import com.digidinos.shopping.entity.Product;
import com.digidinos.shopping.model.CartInfo;
import com.digidinos.shopping.model.CartLineInfo;
import com.digidinos.shopping.model.CustomerInfo;
import com.digidinos.shopping.repository.OrderDetailRepository;
import com.digidinos.shopping.repository.OrderRepository;
import com.digidinos.shopping.repository.ProductRepository;

@Service
public class OrderDetailService {
	@Autowired
	OrderRepository orderRepository;

	@Autowired
	ProductRepository productRepository;
	@Autowired
	OrderDetailRepository orderDetailRepository;

	public List<Order> findAll() {
		return this.orderRepository.findAll();
	}

	public void add(Order order) {
		this.orderRepository.save(order);

	}

//	@Transactional(rollbackFor = Exception.class)
	public void saveOrder(CartInfo cartInfo) {
		int orderNum = orderRepository.maxOrderNum() + 1;
		System.out.println(orderNum);
		Order order = new Order();
		order.setOrderNum(orderNum);
		order.setOrderDate(new Date());
		order.setAmount(cartInfo.getAmountTotal());

		CustomerInfo customerInfo = cartInfo.getCustomerInfo();
		order.setCustomerName(customerInfo.getName());
		order.setCustomerEmail(customerInfo.getEmail());
		order.setCustomerPhone(customerInfo.getPhone());
		order.setCustomerAddress(customerInfo.getAddress());
		System.out.println("Name : " + order.getCustomerName());

		orderRepository.save(order);

		List<CartLineInfo> lines = cartInfo.getCartLines();

		for (CartLineInfo line : lines) {
			OrderDetail detail = new OrderDetail();
			detail.setOrder(order);
			detail.setAmount(line.getAmount());
			detail.setPrice(line.getProductInfo().getPrice());
			detail.setQuanity(line.getQuantity());

			int id = line.getProductInfo().getId();
			Optional<Product> productOpt = productRepository.findById(id);
			Product product = productOpt.get();
			detail.setProduct(product);
			orderDetailRepository.save(detail);
		}

	}

	public Optional<OrderDetail> selectById(int id) {
		Optional<OrderDetail> orderDetail = orderDetailRepository.findById(id);
		return orderDetail;

	}

	public List<OrderDetail> findbyorderid(Integer id) {
		List<OrderDetail> listOrderDetail = orderDetailRepository.findAllByOrderId(id);
		return listOrderDetail;

	}

}
