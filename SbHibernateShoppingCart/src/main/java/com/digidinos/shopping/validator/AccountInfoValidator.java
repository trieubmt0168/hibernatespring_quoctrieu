package com.digidinos.shopping.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.digidinos.shopping.form.AccountInfoForm;
import com.digidinos.shopping.service.AccountService;
@Component
public class AccountInfoValidator implements Validator {

	@Autowired
	AccountService accountService;

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz == AccountInfoForm.class;
	}

	@Override
	public void validate(Object target, Errors errors) {
		AccountInfoForm accountInfoForm = (AccountInfoForm) target;

	}
}
