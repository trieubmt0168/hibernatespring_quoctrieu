package com.digidinos.shopping.validator;

import java.util.regex.Pattern;

import org.springframework.stereotype.Component;
@Component
public class PasswordValidator {
	private Pattern pattern;
	private static final String PASSWORD_PATTERN =  "^[a-z0-9._-]{3,15}$";

	public PasswordValidator() {
		pattern = Pattern.compile(PASSWORD_PATTERN);
	}

	public boolean validate(final String password) {
		return pattern.matcher(password).matches();
	}
}
