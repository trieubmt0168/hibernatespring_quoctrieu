package com.digidinos.shopping.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.digidinos.shopping.entity.Account;
import com.digidinos.shopping.form.AccountForm;
import com.digidinos.shopping.service.AccountService;
@Component
public class AccountUpdateRoleValidator implements Validator {

	@Autowired
	AccountService accountService;

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz == AccountForm.class;
	}

	@Override
	public void validate(Object target, Errors errors) {
		AccountForm accountForm = (AccountForm) target;

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "encrytedPassword", "NotEmpty.accountForm.encrytedPassword");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "verifyPassword", "NotEmpty.accountForm.verifyPassword");

		if (!accountForm.getEncrytedPassword().equals(accountForm.getVerifyPassword())) {
			errors.rejectValue("verifyPassword", "NotMatches.accountForm.verifyPassword");
		}
		String userName = accountForm.getUserName();
		if (accountForm.isNewAccount()) {
			Account account = accountService.findByUserName(userName);
			if (account != null) {
				errors.rejectValue("userName", "Duplicate.accountForm.userName");
			}
		}

	}
}
