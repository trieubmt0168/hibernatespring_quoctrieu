package com.digidinos.shopping.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.digidinos.shopping.entity.Account;
import com.digidinos.shopping.entity.Order;
import com.digidinos.shopping.entity.OrderDetail;
import com.digidinos.shopping.entity.Product;
import com.digidinos.shopping.form.AccountChangePasswordForm;
import com.digidinos.shopping.form.AccountForm;
import com.digidinos.shopping.form.AccountInfoForm;
import com.digidinos.shopping.form.AccountUpdateRoleForm;
import com.digidinos.shopping.form.ProductForm;
import com.digidinos.shopping.model.OrderDetailInfo;
import com.digidinos.shopping.model.OrderInfo;
import com.digidinos.shopping.pagination.Pagination;
import com.digidinos.shopping.service.AccountService;
import com.digidinos.shopping.service.AddAccountService;
import com.digidinos.shopping.service.OrderDetailService;
import com.digidinos.shopping.service.OrderService;
import com.digidinos.shopping.service.ProductService;
import com.digidinos.shopping.validator.AccountChangePasswordValidator;
import com.digidinos.shopping.validator.AccountFormValidator;
import com.digidinos.shopping.validator.AccountInfoValidator;
import com.digidinos.shopping.validator.ProductFormValidator;

@Controller
//@Transactional
public class AdminController {

//    @Autowired
//    private OrderDAO orderDAO;

	@Autowired
	private ProductService productService;
	@Autowired
	private AccountService accountService;
	@Autowired
	private OrderService orderService;
	@Autowired
	private OrderDetailService orderDetailService;
	@Autowired
	private AddAccountService accountService1;
	@Autowired
	private Pagination pagination;
// 
	@Autowired
	private ProductFormValidator productFormValidator;

	@Autowired
	private AccountFormValidator accountFormValidator;

	@Autowired
	private AccountInfoValidator accountInfoValidator;

	@Autowired
	private AccountChangePasswordValidator accountChanePasswordValidator;

	@InitBinder
	public void myInitBinder(WebDataBinder dataBinder) {
		Object target = dataBinder.getTarget();
		if (target == null) {
			return;
		}
		System.out.println("Target=" + target);

		if (target.getClass() == ProductForm.class) {
			dataBinder.setValidator(productFormValidator);
			System.out.println("Product Valid");
		}
		if (target.getClass() == AccountForm.class) {
			dataBinder.setValidator(accountFormValidator);
			System.out.println("Account Valid");
		}
		if (target.getClass() == AccountChangePasswordForm.class) {
			dataBinder.setValidator(accountChanePasswordValidator);
			System.out.println("Account Change Password Valid");
		}
		if (target.getClass() == AccountInfoForm.class) {
			dataBinder.setValidator(accountInfoValidator);
			System.out.println("Account Info Validator Valid");
		}
	}

	// GET: Hiển thị trang login
	@RequestMapping(value = { "/login" }, method = RequestMethod.GET)
	public String login(Model model) {

		return "login";
	}

///////account form
	@RequestMapping(value = { "/admin/accountInfo" }, method = RequestMethod.GET)
	public String accountInfo(Model model) {

		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		AccountInfoForm accountInfoForm = null;
		Account account = accountService.findByUserName(userDetails.getUsername());
		accountInfoForm = new AccountInfoForm(account);
		model.addAttribute("accountInfoForm", accountInfoForm);

		model.addAttribute("userDetails", userDetails);
		return "accountInfo";
	}

	@RequestMapping(value = { "/admin/accountInfo" }, method = RequestMethod.POST)
	public String usertUpdate(Model model,
			@ModelAttribute("accountInfoForm") @Validated AccountInfoForm accountInfoForm, BindingResult result,
			final RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {
			System.out.println(result.getAllErrors());
			return "accountInfo";

		}
		try {
			accountService.updateProfile(accountInfoForm.getAccount());

		} catch (Exception e) {
			Throwable rootCause = ExceptionUtils.getRootCause(e);
			String message = rootCause.getMessage();
			model.addAttribute("errorMessage", message);
			return "accountInfo";
		}
		return "redirect:accountInfo";
	}

	////////////////////////// PRODUCT////////////////////////////////////////////////////
//  Danh sách sản phẩm.
	@RequestMapping(value = { "/admin/productList" }, method = RequestMethod.GET)
	public String listProductHandler(Model model //
		    ,@RequestParam(value = "name", required = false, defaultValue = "") String likeName,
			@RequestParam(value = "name", required = false, defaultValue = "") String likeNamepro,
			@RequestParam(value = "page", required = false, defaultValue = "1") int currentPage,
			@RequestParam(value = "size", required = false, defaultValue = "5") int size) {
		Sort sortable = Sort.by("id").descending();
		List<String> pagingNumber = new ArrayList();
		if (!likeName.isEmpty()) {
			Pageable pageable = PageRequest.of(currentPage - 1, size);
			Page<Product> productPage = productService.findByNameProduct(likeName, pageable);
			pagingNumber = pagination.paginationProduct(productPage, currentPage);
			model.addAttribute("pagination", pagingNumber);
			model.addAttribute("paginationProducts", productPage);
			return "/adminProduct";
		}
		Pageable pageable = PageRequest.of(currentPage - 1, size, sortable);
		Page<Product> productPage = productService.findAll(pageable);
		pagingNumber = pagination.paginationProduct(productPage, currentPage);
		model.addAttribute("searchName", likeName);
		model.addAttribute("pagination", pagingNumber);
		model.addAttribute("paginationProducts", productPage);
		return "adminProduct";
	}

	// GET: Hiển thị product
	@RequestMapping(value = { "/admin/addProduct" }, method = RequestMethod.GET)
	public String product(Model model, @RequestParam(value = "id", defaultValue = "") Integer id) {
		ProductForm productForm = null;

		if (id != null && id > 0) {
			Product product = (Product) productService.findAll();
			if (product != null) {
				productForm = new ProductForm(product);
			}
		}
		if (productForm == null) {
			productForm = new ProductForm();
			productForm.setNewProduct(true);
		}
		model.addAttribute("productForm", productForm);
		return "addProduct";
	}

//	     GET: Hiển thị edit
	@RequestMapping(value = { "/admin/editProduct" }, method = RequestMethod.GET)
	public String product1(Model model, @RequestParam(value = "id", defaultValue = "") Integer id) {
		ProductForm productForm = null;

		if (id != null) {
			Optional<Product> productOpt = productService.selectbyid(id);
			if (productOpt.isPresent()) {
				Product product = productOpt.get();
				productForm = new ProductForm(product);
			}
		}

		if (productForm == null) {
			productForm = new ProductForm();
			productForm.setNewProduct(true);
		}
		model.addAttribute("productForm", productForm);
		return "editProduct";
	}

	// POST: Save product
	@RequestMapping(value = { "/admin/addProduct" }, method = RequestMethod.POST)
	public String productSave(Model model, //
			@ModelAttribute("productForm") @Validated ProductForm productForm, //
			BindingResult result, //
			final RedirectAttributes redirectAttributes) {

		if (result.hasErrors()) {
			return "addProduct";
		}
		try {

			productService.add(productForm.getProduct());
		} catch (Exception e) {
			Throwable rootCause = ExceptionUtils.getRootCause(e);
			String message = rootCause.getMessage();
			model.addAttribute("errorMessage", message);
			// Show product form.
			return "addProduct";
		}

		return "redirect:/admin/productList";
	}

	@RequestMapping(value = { "/admin/editProduct" }, method = RequestMethod.POST)
	public String productSave1(Model model, //
			@ModelAttribute("productForm") @Validated ProductForm productForm, //
			BindingResult result, //
			final RedirectAttributes redirectAttributes) {

		if (result.hasErrors()) {
			return "editProduct";
		}
		try {
			productService.update(productForm.getProduct());
		} catch (Exception e) {
			Throwable rootCause = ExceptionUtils.getRootCause(e);
			String message = rootCause.getMessage();
			model.addAttribute("errorMessage", message);
			// Show product form.
			return "editProduct";
		}

		return "redirect:/admin/productList";
	}

	@RequestMapping(value = { "admin/deleteProduct" }, method = RequestMethod.GET)
	public String removeAccountHandlear(Model model, @RequestParam(value = "id", defaultValue = "") int id) {
		if (id != 0 && id > 0) {
			productService.delete(id);
		}
		return "redirect:/admin/productList";
	}

	//////////////////////// Account//////////////////////////////////////////////
	@RequestMapping(value = { "/admin/accountList" }, method = RequestMethod.GET)
	public String listAccountHandler(Model model,
			@RequestParam(value = "name", required = false, defaultValue = "") String likeName,
			@RequestParam(value = "page", required = false, defaultValue = "1") int currentPage,
			@RequestParam(value = "size", required = false, defaultValue = "5") int size) {
		Sort sortable = Sort.by("id").descending();
		List<String> pagingNumber = new ArrayList();
		if (!likeName.isEmpty()) {
			Pageable pageable = PageRequest.of(currentPage - 1, size);
			Page<Account> accountPage = accountService.findByNameAccount(likeName, pageable);
			pagingNumber = pagination.paginationAccount(accountPage, currentPage);
			model.addAttribute("pagination", pagingNumber);
			model.addAttribute("paginationAccount", accountPage);
			return "/adminAcount";
		}
		Pageable pageable = PageRequest.of(currentPage - 1, size, sortable);
		Page<Account> accountPage = accountService.findAll(pageable);
		pagingNumber = pagination.paginationAccount(accountPage, currentPage);
		model.addAttribute("pagination", pagingNumber);
		model.addAttribute("paginationAccount", accountPage);
		return "adminAcount";
	}

	// GET: Hiển thị account
	@RequestMapping(value = { "/admin/addAccount" }, method = RequestMethod.GET)
	public String account(Model model, @RequestParam(value = "id", defaultValue = "") Integer id) {
		AccountForm accountForm = null;

		if (id != null && id > 0) {
			Account account = (Account) accountService.findAll();
			if (account != null) {
				accountForm = new AccountForm(account);
			}
		}
		if (accountForm == null) {
			accountForm = new AccountForm();
			accountForm.setNewAccount(true);
		}
		model.addAttribute("accountForm", accountForm);
		return "addAccount";
	}

	// POST: Save product
	@RequestMapping(value = { "/admin/addAccount" }, method = RequestMethod.POST)
	public String accountSave(Model model, //
			@ModelAttribute("accountForm") @Validated AccountForm accountForm, //
			BindingResult result, //
			final RedirectAttributes redirectAttributes) {

		if (result.hasErrors()) {
			return "addAccount";
		}
		try {

			accountService.add(accountForm.getAccount3());
		} catch (Exception e) {
			Throwable rootCause = ExceptionUtils.getRootCause(e);
			String message = rootCause.getMessage();
			model.addAttribute("errorMessage", message);
			// Show product form.
			return "addAccount";
		}

		return "redirect:/admin/accountList";
	}

//		     GET: Hiển thị edit
	@RequestMapping(value = { "/admin/editAccount" }, method = RequestMethod.GET)
	public String accountUpdate(Model model, @RequestParam(value = "id", defaultValue = "") Integer id) {
		AccountUpdateRoleForm accountForm = null;

		if (id != null) {
			Optional<Account> accountOpt = accountService.selectbyid(id);
			if (accountOpt.isPresent()) {
				Account account = accountOpt.get();
				accountForm = new AccountUpdateRoleForm(account);
			}
		}
		model.addAttribute("accountUpdateRoleForm", accountForm);
		return "editAccount";
	}

	@RequestMapping(value = { "/admin/editAccount" }, method = RequestMethod.POST)
	public String accountSave(Model model, //
			@ModelAttribute("accountUpdateRoleForm") @Validated AccountUpdateRoleForm accountForm, //
			BindingResult result, //
			final RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {
			return "editAccount";
		}
		try {
			System.out.println(accountForm.getAccount());
			accountService.updateRole(accountForm.getAccount());
		} catch (Exception e) {
			Throwable rootCause = ExceptionUtils.getRootCause(e);
			String message = rootCause.getMessage();
			model.addAttribute("errorMessage", message);
			// Show product form.
			return "editAccount";
		}

		return "redirect:/admin/accountList";
	}

	@RequestMapping(value = { "admin/deleteaccount" }, method = RequestMethod.GET)
	public String removeAccountHandler(Model model, @RequestParam(value = "id", defaultValue = "") int id) {
		if (id != 0 && id > 0) {
			accountService.delete(id);
		}
		return "redirect:/admin/accountList";
	}
	//////////////////////////////////// Order///////////////////////////////////////////////////////////

	// GET : Hiển thị Order List
	@RequestMapping(value = { "/admin/orderList" }, method = RequestMethod.GET)
	public String orderList(Model model, @RequestParam(value = "page", required = false, defaultValue = "0") int page,
			@RequestParam(value = "name", required = false, defaultValue = "") String likeName,
			@RequestParam(value = "page", required = false, defaultValue = "1") int currentPage,
			@RequestParam(value = "size", required = false, defaultValue = "8") int size) {
		Sort sortable = Sort.by("id").descending();
		List<String> pagingNumber = new ArrayList();
		if (!likeName.isEmpty()) {
			Pageable pageable = PageRequest.of(currentPage - 1, size);
			Page<Order> orderPage = orderService.findByNameOrder(likeName, pageable);
			pagingNumber = pagination.paginationOrder(orderPage, currentPage);
			model.addAttribute("pagination", pagingNumber);
			model.addAttribute("paginationOrder", orderPage);
			return "/orderList";
		}
		Pageable pageable = PageRequest.of(currentPage - 1, size, sortable);
		Page<Order> orderPage = orderService.findAll(pageable);
		pagingNumber = pagination.paginationOrder(orderPage, currentPage);
		model.addAttribute("pagination", pagingNumber);
		model.addAttribute("paginationOrder", orderPage);

		return "orderList";
	}

	//////
	@RequestMapping(value = { "/admin" }, method = RequestMethod.GET)
	public String Adminhome(Model model) {

		return "adminHome";
	}

	@RequestMapping(value = { "/admin/order" }, method = RequestMethod.GET)
	public String orderView(Model model, @RequestParam("id") Integer id) {
		System.out.println("IDDD" + id);
		OrderInfo orderInfo = null;
		if (id != null) {
			Optional<Order> orderOpt = orderService.findbyid(id);
			if (orderOpt.isPresent()) {
				Order order = orderOpt.get();
				orderInfo = new OrderInfo(order.getId(), order.getOrderDate(), order.getOrderNum(), order.getAmount(),
						order.getCustomerName(), order.getCustomerAddress(), order.getCustomerEmail(),
						order.getCustomerPhone());
			}
			System.out.println("Order INFOO : " + orderInfo.getOrderNum());
		}
		if (orderInfo == null) {
			return "redirect:/admin/orderList";
		}
		List<OrderDetailInfo> listOrderDetailInfo = new ArrayList<>();
		List<OrderDetail> listOrderDetails = orderDetailService.findbyorderid(id);
		for (OrderDetail orderDetail : listOrderDetails) {
			OrderDetailInfo orderDetailInfo = new OrderDetailInfo(orderDetail.getId(), orderDetail.getProduct().getId(),
					orderDetail.getProduct().getName(), orderDetail.getQuanity(), orderDetail.getPrice(),
					orderDetail.getAmount());
			listOrderDetailInfo.add(orderDetailInfo);
		}
		orderInfo.setDetails(listOrderDetailInfo);

		System.out.println("Order INFOO Sau : " + orderInfo.getOrderNum());

		model.addAttribute("orderInfo", orderInfo);

		return "order";
	}

	//////////////////////////////// Changger
	//////////////////////////////// password/////////////////////////////////////////////////

	@RequestMapping(value = { "/admin/changePassword" }, method = RequestMethod.GET)
	public String userChangePass(Model model) {
		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		AccountChangePasswordForm accountChangeForm = null;
		Account account = accountService.findByUserName(userDetails.getUsername());
		accountChangeForm = new AccountChangePasswordForm(account);
		accountChangeForm.setNewPassword(true);
		model.addAttribute("accountChangePasswordForm", accountChangeForm);
		return "changePassword";
	}

	// POST : Update Changepassword
	@RequestMapping(value = { "/admin/changePassword" }, method = RequestMethod.POST)
	public String usertChangePass(Model model,
			@ModelAttribute("accountChangePasswordForm") @Validated AccountChangePasswordForm accountChangePasswordForm,
			BindingResult result, final RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {
			System.out.println("AAAAAAAAAAAAAAAAA" + result);
			return "/changePassword";
		}
		try {
			accountService.updatePassword(accountChangePasswordForm.getAccount());
		} catch (Exception e) {
			Throwable rootCause = ExceptionUtils.getRootCause(e);
			String message = rootCause.getMessage();
			model.addAttribute("errorMessage", message);
			return "changePassword";
		}
		return "redirect:accountInfo";
	}

}
