package com.digidinos.shopping;

import java.util.Arrays;
import java.util.stream.Collectors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.digidinos.shopping.entity.Product;
import com.digidinos.shopping.service.ProductService;
import com.github.javafaker.Faker;
@Component
public class MigrateDataCommandRunner implements CommandLineRunner {
	protected final Log logger = LogFactory.getLog(getClass());

	@Autowired
	private ProductService prodService;

	public void run(String... args) throws Exception {
		String strArgs = Arrays.stream(args).collect(Collectors.joining("|"));
		logger.info("Application started with arguments:" + strArgs);

		if (strArgs.equals("migrate")) {
			migratedData();
		}
	}

	protected void migratedData() {
		logger.info("Application started migratedData");

		for (int i = 0; i < 1000; i++) {
			Product entity = new Product();
			Faker faker = new Faker();
			entity.setCode(faker.country().countryCode2());
			entity.setName(faker.company().name());
			entity.setDescription(faker.book().title());
			entity.setPrice((double) (Math.random() * 200 + 1));

			prodService.add(entity);
		}
	}
}